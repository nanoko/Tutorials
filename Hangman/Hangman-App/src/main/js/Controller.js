/**
 * Copyright ® 2008-2012, Ubiquitous Dreams SARL - SIREN 508 175 437 RCS La Rochelle.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var scope = (scope == null ? window : this);
scope.Controller = (scope.Controller == null ? {} : scope.Controller);

scope.Controller = {

    /* Local attributes */
    hub: null,

    /**
     * Method returning the component <b>unique</b>
     * name. Using a fully qualified name is encouraged.
     * @return the component unique name
     */
    getComponentName: function() {
        return 'controller-component';
    },

    /**
     * Configure method. This method is called when the
     * component is registered on the hub.
     * @param aHub the hub
     * @param configure the JSON object containing parameters
     */
    configure: function(aHub, configure) {
        this.hub = aHub;
    },

    /**
     * The Start function
     * This method is called when the hub starts or just
     * after configure if the hub is already started.
     */
    start: function() {

    },

    /**
     * The Stop method is called when the hub stops or
     * just after the component removal if the hub is
     * not stopped. No events can be send in this method.
     */
    stop: function() {

    }

    /* Public methods (contract implementation) */
}