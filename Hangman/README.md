#Nanoko tutorial application

Nanoko is a JavaScript modular framework enabling to build flexible and sustainable HTML5 applications that can be deployed on both mobile and desktop applications.

Follow the tutorial on our [website](http://www.nanoko.org/?page=gettingstarted) !
